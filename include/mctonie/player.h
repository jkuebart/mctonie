// Copyright 2023 Joachim Kuebart <joachim.kuebart@gmail.com>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the
//      distribution.
//
//   3. Neither the name of the copyright holder nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MCTONIE_PLAYER_H
#define MCTONIE_PLAYER_H

#include <AudioFileSourceSD.h>
#include <AudioGeneratorMP3.h>
#include <AudioOutputI2S.h>

namespace mctonie
{

/// Play all files in a directory in sequence.
///
/// A starting index may be provided. Playback ends when all files have
/// been played.
class Player
{
public:
    Player();
    Player(Player const&) = delete;
    Player& operator=(Player const&) = delete;

    /// Perform playing.
    void Loop();

    /// @param dir_name The directory from which to play files.
    /// @param index The starting index in the directory.
    void Play(String dir_name, unsigned index);

    /// @return The index of the *next* file to be played.
    unsigned Index() const;

    /// Stop playing.
    void Stop();

private:
    AudioFileSourceSD source_;
    AudioOutputI2S audio_out_;
    AudioGeneratorMP3 generator_;
    String dir_name_;
    unsigned index_{0};
    std::vector<String> files_;
};

} // namespace mctonie

#endif // MCTONIE_PLAYER_H
