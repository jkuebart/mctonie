// Copyright 2023 Joachim Kuebart <joachim.kuebart@gmail.com>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the
//      distribution.
//
//   3. Neither the name of the copyright holder nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MCTONIE_CARD_READER_H
#define MCTONIE_CARD_READER_H

#include <MFRC522DriverPinSimple.h>
#include <MFRC522DriverSPI.h>
#include <MFRC522v2.h>

namespace mctonie
{

/// Detect when PICCs enter or leave the RFID reader.
///
/// In order to avoid time-consuming SELECTs, we use the following
/// approximation:
///   - If we have a UID and there is no response to WUPA (wakeup), we
///     clear the UID.
///   - If there is a response to REQA (request), we SELECT the PICC,
///     store its UID and read the index. Then we HLTA (halt) the PICC to
///     make it respond to WUPA (wakeup) again.
class CardReader
{
    using TUid = MFRC522::Uid;

    /// A scope guard for an authenticated connection.
    class Authenticated;

public:
    /// @param cs_pin Use the given pin as chip-select.
    explicit CardReader(std::uint8_t cs_pin = PIN_SPI_SS);
    CardReader(CardReader const&) = delete;
    CardReader& operator=(CardReader const&) = delete;

    /// @return Whether MFRC522 was initialised successfully.
    bool Init();

    /// Update the UID based on the PICC on the reader.
    void Loop();

    /// @return Whether a PICC is currently in range and hasn't been
    /// cleared.
    bool HasUid() const;

    /// @return The UID of the current PICC.
    String Uid() const;

    /// @return The index stored in the current PICC, or 0 if the PICC
    /// isn't initialised.
    unsigned Index() const;

    /// Store the given index on the PICC if it has been initialised.
    /// @param index The index.
    /// @return Whether the index was stored successfully.
    bool Index(unsigned index);

private:
    MFRC522DriverPinSimple cs_pin_;
    MFRC522DriverSPI rfid_driver_;
    MFRC522 mfrc522_;
    TUid uid_;
    unsigned index_;
};

} // namespace mctonie

#endif // MCTONIE_CARD_READER_H
