McTonie
=======

This project implements an RFID-controlled music player.

The setup consists of an RFID reader, an audio DAC and an SD card reader
connected to a central microcontroller. When an RFID token appears within
range of the receiver, the player retrieves its unique ID and begins to
play audio files stored in a directory with that name on the SD card.
Playback stops when the token is removed. The number of the next file to be
played is stored on the token and playback resumes at that position the
next time.

While this project is very similar to [TonUINO][T], its design allows
slightly more flexibility. Instead of a self-contained [MP3 player][DF] it
uses an ordinary [DAC module][PCM5102] as an output device.

  - Because decoding happens in software, a [wide variety of
    formats][AUDIO] such as AAC, FLAC, MIDI, MOD, MP3, Opus and WAV can be
    supported. Only MP3 is currently supported.

  - Voice output is possible. This feature is not yet implemented.

  - If the microcontroller provides WiFi, web radio stations can be played.
    This feature is not yet implemented.

  - The player can play podcasts. This feature is not yet implemented.

  - It is possible to resume playback where it was interrupted, rather than
    just from the start of a file. This feature is not yet implemented.

  - Audio files can be uploaded via WiFi. This feature is not yet
    implemented.


Parts
-----

The design has been tested with the following parts:

  - Microcontroller [WeMos D1 mini][D1]
  - RFID reader/writer [MFRC522][MFRC522]
  - [SD card shield][SD]
  - DAC based on [PCM5102A][DAC]

Supported RFID tokens are MIFARE Ultralight and Ultralight C and MIFARE
Classic. The latter tokens use the default key of 0xffffffffffff.


Connections
-----------

Apart from power supply, the following connections are required:

|               |ESP8266| D1 mini, NodeMCU      |
|---------------|-------|-----------------------|
| **DAC**       |       |                       |
| BCLK          |GPIO15 | D8                    |
| DIN           |GPIO3  | RX                    |
| LRCLK         |GPIO2  | D4                    |
| **RC522**     |       |                       |
| CS            |GPIO5  | D1                    |
| MISO          |GPIO12 | D6                    |
| MOSI          |GPIO13 | D7                    |
| SCLK          |GPIO14 | D5                    |
| **SD card**   |       |                       |
| CS            |GPIO16 | D0, see below         |
| MISO          |GPIO12 | D6                    |
| MOSI          |GPIO13 | D7                    |
| SCLK          |GPIO14 | D5                    |

> **Note** The D1 mini SD card shield connects to D8 (GPIO15) as
> chip-select, but this has to be changed because GPIO15 is part of the I2S
> interface to the DAC. The SD card shield's D8 pin must be rewired to D0
> (GPIO16).


Usage
-----

The default `esp8266` build can be compiled and flashed using

    pio run --target upload

However, the resume position is only stored on tokens that have been
explicitly initialised to prevent accidental data destruction.

Token initialisation is the purpose of the `esp8266_init` build that
unconditionally initialises any RFID token. It can be compiled and flashed
using

    pio run --environment esp8266_init --target upload

This build also prints the token's unique ID on the serial console which
should be used for the directory names on the SD card.

After all RFID tokens have been initialised and their IDs noted, the
default build should be flashed for regular operation.

The SD card should be formatted with FAT16, FAT32 or exFAT. The audio files
should be placed in the directory

    /McTonie/<UID>/*.mp3

where UID is the unique ID of the RFID token with which the files should be
associated.


TODO
----

  - There should be a user interface for initialising RFID tags. Because
    unknown tags are never modified, they can't store the resume position.
    Initialisation requires a build with the `MCTONIE_INIT` macro which
    overwrites any tag, thereby potentially destroying existing
    information.


Images
------

![The circuit on a breadboard in front of the speaker
box.](docs/00-experiment.jpeg){width=50%}

Experimental setup

![D1 mini with SD card shield and audio DAC on a strip board seen from the
top.](docs/01-top.jpeg){width=50%}

Top view

![D1 mini with SD card shield and audio DAC on a strip board seen from
below showing the wiring.](docs/02-bottom.jpeg){width=50%}

Bottom view

![The strip board squeezed into the speaker box
case.](docs/03-speaker.jpeg){width=50%}

A squash and a squeeze


[AUDIO]: https://github.com/earlephilhower/ESP8266Audio
[D1]: https://www.az-delivery.de/products/d1-mini
[DAC]: https://www.aliexpress.com/item/1005005393398013.html
[DF]: https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299
[MFRC522]: https://www.az-delivery.de/products/rfid-set
[PCM5102]: https://www.ti.com/product/PCM5102A
[SD]: https://www.az-delivery.de/products/micro-sd-card-adapter-shield
[T]: https://github.com/tonuino/TonUINO-TNG
