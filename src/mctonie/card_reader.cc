// Copyright 2023 Joachim Kuebart <joachim.kuebart@gmail.com>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the
//      distribution.
//
//   3. Neither the name of the copyright holder nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "mctonie/card_reader.h"

#include "mctonie/log.h"

#include <MFRC522Debug.h>
#include <lwip/inet.h>

using MIFARE_Key = MFRC522::MIFARE_Key;
using PCD_Register = MFRC522::PCD_Register;
using PICC_Command = MFRC522::PICC_Command;
using PICC_Type = MFRC522::PICC_Type;
using StatusCode = MFRC522::StatusCode;

namespace
{

/// The key used for PICCs.
MIFARE_Key kKey{{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}};

/// The block address where we store data.
constexpr byte kBlockAddress{4};

/// The block size plus CRC.
constexpr std::size_t kBlockSize{16};
constexpr std::size_t kCrcSize{2};

/// The signature to identify our PICCs.
constexpr union Signature
{
    byte bytes[4];
    std::uint32_t value;
} kSignature{{'M', 'C', 'T', 'N'}};

/// The format of our storage.
struct Storage
{
    union
    {
        struct
        {
            Signature signature;
            std::uint32_t index;
        };
        struct
        {
            byte buffer[kBlockSize];
            byte crc[kCrcSize];
        };
    };
    byte size{sizeof(buffer) + sizeof(crc)};
};

/// Only PICCs which have just powered up and are IDLE respond to kRequest.
/// PICCs in states HALT or IDLE respond to kWakeup.
enum class RequestType
{
    kRequest,
    kWakeup
};

bool RequestOrWakeup(
    RequestType const request_type,
    MFRC522Driver& rfid_driver,
    MFRC522& mfrc522)
{
    // There are strict timings for REQA and WUPA: the PICC has at most 9
    // bits to respond with 16 bits (ISO/IEC 14443-3 6.1.2).
    rfid_driver.PCD_WriteRegister(PCD_Register::TReloadRegH, 0);
    rfid_driver.PCD_WriteRegister(PCD_Register::TReloadRegL, 9 + 16);

    byte buffer[2];
    byte buffer_size{sizeof(buffer)};
    StatusCode const result{
        RequestType::kRequest == request_type
            ? mfrc522.PICC_RequestA(buffer, &buffer_size)
            : mfrc522.PICC_WakeupA(buffer, &buffer_size)};
    return StatusCode::STATUS_OK == result ||
        StatusCode::STATUS_COLLISION == result;
}

bool Request(MFRC522Driver& rfid_driver, MFRC522& mfrc522)
{
    return RequestOrWakeup(RequestType::kRequest, rfid_driver, mfrc522);
}

bool Wakeup(MFRC522Driver& rfid_driver, MFRC522& mfrc522)
{
    return RequestOrWakeup(RequestType::kWakeup, rfid_driver, mfrc522);
}

StatusCode WriteStorage(MFRC522& mfrc522, byte const sak, Storage& storage)
{
    if (PICC_Type::PICC_TYPE_MIFARE_UL == mfrc522.PICC_GetType(sak)) {
        // MIFARE Ultralight can only write 4 bytes at a time.
        constexpr unsigned kWriteSize{4};
        for (unsigned index{0}; index != sizeof(storage.buffer) / kWriteSize;
             ++index)
        {
            StatusCode const status{mfrc522.MIFARE_Ultralight_Write(
                kBlockAddress + index,
                storage.buffer + index * kWriteSize,
                kWriteSize)};
            if (StatusCode::STATUS_OK != status) {
                return status;
            }
        }
        return StatusCode::STATUS_OK;
    }

    return mfrc522.MIFARE_Write(
        kBlockAddress,
        storage.buffer,
        sizeof(storage.buffer));
}

bool UpdateIndex(MFRC522& mfrc522, byte const sak, unsigned const index)
{
    Storage storage{kSignature, htonl(index)};

    if (StatusCode::STATUS_OK != WriteStorage(mfrc522, sak, storage)) {
        LOG_ERROR("Write error.");
        return false;
    }
    LOG_DEBUG("Index updated to %u.", index);
    return true;
}

} // namespace

namespace mctonie
{

class CardReader::Authenticated
{
public:
    explicit Authenticated(CardReader& card_reader);
    Authenticated(Authenticated const&) = delete;
    Authenticated& operator=(Authenticated const&) = delete;
    ~Authenticated();

    explicit operator bool() const;

private:
    CardReader& card_reader_;

    enum class State
    {
        kFailed,
        kSelected,
        kEncrypted
    };
    State state_;
};

CardReader::Authenticated::Authenticated(CardReader& card_reader)
: card_reader_{card_reader}
, state_{State::kFailed}
{
    constexpr unsigned kTimeoutBits{2048};

    // Use relaxed timing (20ms) for the authentication process.
    card_reader_.rfid_driver_.PCD_WriteRegister(
        PCD_Register::TReloadRegH,
        kTimeoutBits / 0x100);
    card_reader_.rfid_driver_.PCD_WriteRegister(
        PCD_Register::TReloadRegL,
        kTimeoutBits % 0x100);

    // SELECT the PICC and store its UID.
    if (StatusCode::STATUS_OK !=
        card_reader_.mfrc522_.PICC_Select(
            &card_reader_.uid_,
            8 * card_reader_.uid_.size))
    {
        LOG_ERROR("SELECT error.");
        return;
    }
    state_ = State::kSelected;

    // If it's not a MIFARE Classic card, we're finished.
    PICC_Type const type{
        card_reader_.mfrc522_.PICC_GetType(card_reader_.uid_.sak)};
    if (type < PICC_Type::PICC_TYPE_MIFARE_MINI ||
        PICC_Type::PICC_TYPE_MIFARE_4K < type)
    {
        return;
    }

    // Authenticate our data block.
    if (StatusCode::STATUS_OK !=
        card_reader_.mfrc522_.PCD_Authenticate(
            PICC_Command::PICC_CMD_MF_AUTH_KEY_A,
            kBlockAddress,
            &kKey,
            &card_reader_.uid_))
    {
        LOG_ERROR("Authentication error.");
        card_reader_.mfrc522_.PICC_HaltA();
        state_ = State::kFailed;
        return;
    }

    state_ = State::kEncrypted;
}

CardReader::Authenticated::~Authenticated()
{
    if (State::kFailed != state_) {
        card_reader_.mfrc522_.PICC_HaltA();
    }
    if (State::kEncrypted == state_) {
        card_reader_.mfrc522_.PCD_StopCrypto1();
    }
}

CardReader::Authenticated::operator bool() const
{
    return State::kFailed != state_;
}

CardReader::CardReader(std::uint8_t const cs_pin)
: cs_pin_{cs_pin}
, rfid_driver_{cs_pin_}
, mfrc522_{rfid_driver_}
, uid_{}
, index_{0}
{}

bool CardReader::Init()
{
    if (!mfrc522_.PCD_Init()) {
        return false;
    }
    if (!mfrc522_.PCD_PerformSelfTest()) {
        LOG_ERROR("Failed self test.");
    }
    if (!mfrc522_.PCD_Init()) {
        return false;
    }
    MFRC522Debug::PCD_DumpVersionToSerial(mfrc522_, Serial);

    // f_timer = 13.56 MHz / (2 * TPreScaler + 1) where TPreScaler =
    // TModeReg[3..0]:TPrescalerReg, ie 64 => f_timer = 106kHz, the bit
    // frequency.
    // TAuto = 1; timer starts automatically at the end of the transmission
    // in all communication modes at all speeds.
    rfid_driver_.PCD_WriteRegister(PCD_Register::TModeReg, 0x80);
    rfid_driver_.PCD_WriteRegister(PCD_Register::TPrescalerReg, 64);

    return true;
}

void CardReader::Loop()
{
    // If we have a UID and there is no response to WUPA (wakeup), we clear the
    // UID.
    if (HasUid()) {
        if (Wakeup(rfid_driver_, mfrc522_)) {
            mfrc522_.PICC_HaltA();
        } else {
            LOG_DEBUG("No response to WUPA, clearing %s.", Uid().c_str());
            uid_ = {};
        }
        return;
    }

    // Only continue if there is a new PICC in IDLE state.
    if (!Request(rfid_driver_, mfrc522_)) {
        return;
    }

    Authenticated const authenticated{*this};
    if (!authenticated) {
        uid_ = {};
        return;
    }
    LOG_DEBUG("Received PICC's UID: %s.", Uid().c_str());

    // Read our data block from storage.
    Storage storage{};
    if (StatusCode::STATUS_OK !=
            mfrc522_
                .MIFARE_Read(kBlockAddress, storage.buffer, &storage.size) ||
        storage.size < sizeof(storage.buffer))
    {
        LOG_ERROR("Read error (size %i).", storage.size);
        uid_ = {};
    } else if (kSignature.value != storage.signature.value) {
        LOG_ERROR(
            "Invalid signature %08x, using index 0.",
            storage.signature.value);
        index_ = 0;

#ifdef MCTONIE_INIT
        if (UpdateIndex(mfrc522_, uid_.sak, 0)) {
            LOG_DEBUG("PICC initialised.");
        }
#endif // MCTONIE_INIT
    } else {
        index_ = ntohl(storage.index);
    }
    LOG_DEBUG("Retrieved index %u.", index_);

#ifdef MCTONIE_DEBUG_MEMORY
    MFRC522Debug::PICC_DumpToSerial(mfrc522_, Serial, &uid_);
#endif // MCTONIE_DEBUG
}

bool CardReader::HasUid() const
{
    return 0 != uid_.size;
}

String CardReader::Uid() const
{
    String result;
    for (byte i{0}; i != uid_.size; ++i) {
        result += String{uid_.uidByte[i] / 16, HEX};
        result += String{uid_.uidByte[i] % 16, HEX};
    }
    return result;
}

unsigned CardReader::Index() const
{
    return index_;
}

bool CardReader::Index(unsigned const index)
{
    if (index_ == index) {
        return true;
    }

    // Even if updating the index fails, report back the desired index to
    // avoid constant updating.
    index_ = index;
    if (!Wakeup(rfid_driver_, mfrc522_)) {
        LOG_ERROR("No PICC: cannot update index.");
        return false;
    }
    if (Authenticated authenticated{*this}) {
        return UpdateIndex(mfrc522_, uid_.sak, index);
    }
    return false;
}

} // namespace mctonie
