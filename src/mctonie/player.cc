// Copyright 2023 Joachim Kuebart <joachim.kuebart@gmail.com>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the
//      distribution.
//
//   3. Neither the name of the copyright holder nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "mctonie/player.h"

#include "mctonie/log.h"

namespace
{

/// @param A directory name.
/// @param ext extension of files to collect.
/// @return A sorted list of files in the directory.
std::vector<String> FileList(String const& name, String const& ext)
{
    std::vector<String> result;
    for (fs::Dir dir{SDFS.openDir(name)}; dir.next();) {
        if (dir.isFile() && dir.fileName().endsWith(ext)) {
            result.push_back(dir.fileName());
        }
    }
    std::sort(result.begin(), result.end());
    return result;
}

} // namespace

namespace mctonie
{

Player::Player() = default;

void Player::Loop()
{
    // Play to the end of the file.
    if (generator_.isRunning()) {
        if (generator_.loop()) {
            return;
        }
        LOG_DEBUG("Playback ended.");
        generator_.stop();
    }

    // If there are no more files, we're finished.
    if (files_.size() <= index_) {
        return;
    }

    // Start playing the next file.
    String const file_name{dir_name_ + "/" + files_[index_]};
    if (!source_.open(file_name.c_str())) {
        LOG_ERROR("Failed to open %s.", file_name.c_str());
    } else if (!generator_.begin(&source_, &audio_out_)) {
        LOG_ERROR("Cannot start decoder for %s.", file_name.c_str());
    } else {
        LOG_DEBUG("Playing index %u: %s.", index_, file_name.c_str());
    }

    ++index_;
}

void Player::Play(String dir_name, unsigned index)
{
    Stop();
    dir_name_ = std::move(dir_name);
    files_ = FileList(dir_name_, ".mp3");
    LOG_DEBUG(
        "Playing %s from index %u, found %u files.",
        dir_name_.c_str(),
        index,
        files_.size());
    index_ = index;
    if (!files_.empty()) {
        index_ %= files_.size();
    }
}

unsigned Player::Index() const
{
    return files_.empty() ? index_ : index_ % files_.size();
}

void Player::Stop()
{
    if (generator_.isRunning()) {
        generator_.stop();
    }
    files_.clear();
}

} // namespace mctonie
