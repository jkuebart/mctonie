// Copyright 2023 Joachim Kuebart <joachim.kuebart@gmail.com>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the
//      distribution.
//
//   3. Neither the name of the copyright holder nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "mctonie/card_reader.h"
#include "mctonie/log.h"
#include "mctonie/player.h"

#include <SDFS.h>

namespace
{

constexpr std::uint8_t kPinCsRfid{5};
constexpr std::uint8_t kPinCsSd{16};

#ifdef MCTONIE_DEBUG_TIMING
unsigned rfid_time{0};
unsigned music_time{0};
#endif // MCTONIE_DEBUG_TIMING

// Check presence/absence of PICC in this interval.
constexpr unsigned long kPiccCheckMs{200};

unsigned long picc_check_time{0};

mctonie::CardReader card_reader{kPinCsRfid};
std::unique_ptr<mctonie::Player> player;

} // namespace

void setup()
{
    Serial.begin(115200);
    while (!Serial) {
        yield();
    }
    audioLogger = &Serial;

    LOG_DEBUG("CPU freq %u MHz.", ESP.getCpuFreqMHz());

    if (!SDFS.setConfig(SDFSConfig{kPinCsSd}) || !SDFS.begin()) {
        LOG_ERROR("SD card initialisation failed.");
    } else {
        LOG_DEBUG("SD card initialised.");
    }

    if (!card_reader.Init()) {
        LOG_ERROR("RFID initialisation failed.");
    }
}

void loop()
{
    unsigned long const current_time{millis()};
    if (picc_check_time < current_time) {
        picc_check_time = current_time + kPiccCheckMs;

#ifdef MCTONIE_DEBUG_TIMING
        rfid_time -= micros();
#endif // MCTONIE_DEBUG_TIMING

        // Start playing when a new PICC appears in range.
        card_reader.Loop();
        if (card_reader.HasUid()) {
            if (player) {
                // Update the play index while PICC remains on the reader.
                card_reader.Index(player->Index());
            } else {
                player.reset(new mctonie::Player);
                player->Play(
                    "/McTonie/" + card_reader.Uid(),
                    card_reader.Index());
            }
        } else if (player) {
            LOG_DEBUG("Stopping playback.");
            player.reset();
        }

#ifdef MCTONIE_DEBUG_TIMING
        rfid_time += micros();
        LOG_DEBUG(
            "RFID time: %u us, music time: %u us.",
            rfid_time,
            music_time);
        rfid_time = music_time = 0;
#endif // MCTONIE_DEBUG_TIMING
    }

#ifdef MCTONIE_DEBUG_TIMING
    music_time -= micros();
#endif // MCTONIE_DEBUG_TIMING
    if (player) {
        player->Loop();
    }
#ifdef MCTONIE_DEBUG_TIMING
    music_time += micros();
#endif // MCTONIE_DEBUG_TIMING
}
